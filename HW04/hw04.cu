#include <stdlib.h>
#include <stdio.h>
#include "cuda.h"
#include <math.h>

__global__ void addVectorsKernel(int N, float *c_x, float *c_y, float *c_z){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  // evaluate array index "n" for this thread
  int n = t + b*B;
  
  if(n<N){ // only add entries if "n" a legal index
    c_z[n] = c_x[n] + c_y[n];
  }

}

__global__ void sqrtKernel(int N, float *c_z, float *sqrt_ans){
  
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<N){

	 sqrt_ans[n] = sqrt(c_z[n]);
  } 

}

__global__ void sinKernel(int N, float *c_z,float *sin_ans){
  
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<N){
    sin_ans[n] = sin(c_z[n]);
  }

}

__global__ void reverseVectorsKernel(int N, float *c_x, float* c_y, float *rev_ans){
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;
  if(n<N){
    rev_ans[n] = c_x[N-1-n];
  }
}

// HOST main function
int main(int argc, char **argv){
 
  cudaSetDevice(1);

  if(argc<2){ printf("usage: ./cudaAddVectors N\n"); exit(-1); }
  
  // read command line argument
  int N = atoi(argv[1]);

  // allocate an array on the HOST
  float *h_x = (float*) malloc(N*sizeof(float));
  float *h_y = (float*) malloc(N*sizeof(float));
  float *h_z = (float*) malloc(N*sizeof(float));

  // fill values of h_x and h_y on HOST
  int n;
  for(n=0;n<N;++n){
    h_x[n] = drand48();
    h_y[n] = drand48();
  }
  
  //answers for calculations done on HOST
  float *addition_results = (float*) malloc(N*sizeof(float));
  float *sqrt_results = (float*) malloc(N*sizeof(float));
  float *sin_results = (float*) malloc(N*sizeof(float));

  for (n=0; n<N; n++){
    addition_results[n] = h_x[n] + h_y[n];
    sqrt_results[n] = sqrt(addition_results[n]);
    sin_results[n] = sin(sqrt_results[n]);
  }

  // allocate an array on the DEVICE
  float *c_x, *c_y, *c_z;
  cudaMalloc(&c_x, N*sizeof(float));
  cudaMalloc(&c_y, N*sizeof(float));
  cudaMalloc(&c_z, N*sizeof(float));

  // copy data from HOST to DEVICE 
  cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_y, h_y, N*sizeof(float), cudaMemcpyHostToDevice);
  
  // call kernel to set values in array
  int B = 256;
  int G = (N+B-1)/B;


  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  for (n = 0; n < 5; ++n){
:wq

  cudaEventRecord(start); 
  addVectorsKernel <<< G , B >>> (N,  c_x, c_y, c_z);
  cudaEventRecord(stop);
 
  cudaEventRecord(start);
  sqrtKernel <<< G, B >>> (N, c_z, c_x);
  cudaEventRecord(stop);

  cudaEventRecord(start);
  sinKernel <<< G, B >>> (N, c_x, c_y);
  cudaEventRecord(stop);
  }
  // copy data from DEVICE to HOST (blocking)
  cudaMemcpy(h_z, c_z, N*sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_x, c_x, N*sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_y, c_y, N*sizeof(float), cudaMemcpyDeviceToHost);

  // print out result
  float l1Diff = 0;
  float l1DiffSqrt = 0;
  float l1DiffSin = 0;
  for(n=0;n<N;++n){
    l1Diff += fabs(h_z[n] - addition_results[n]);
    l1DiffSqrt += fabs(h_x[n] - sqrt_results[n]);
    l1DiffSin += fabs(h_y[n] - sin_results[n]);
  }


 printf("N=%d, G=%d, B=%d, G*B=%d\n", N, G, B, G*B);

 printf("L1 Diff addition=%17.15e\n", l1Diff);
 printf("L1 Diff sqrt=%17.15e\n", l1DiffSqrt);
 printf("L1 Diff sin=%17.15e\n", l1DiffSin);
  
 return 0;
}

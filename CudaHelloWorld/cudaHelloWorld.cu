#include <stdio.h>
#include <stdlib.h>

__global__ void helloWorldKernel(){
    
    int t = threadIdx.x;

    int b = blockIdx.x;

    printf("Hello world from GPU (thread %d in block %d)!\n", t, b);

}

int main(int argc, char **argv){

    //launch a kernel using one thread-block and one thread
    helloWorldKernel <<< 2, 8 >>> ();

    //make sure print statements flush to stdout
    cudaDeviceSynchronize();

    return 1;
}

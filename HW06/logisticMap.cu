#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

__global__ void logisticMap(int N, int Nit, float *c_x, float *c_r){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if (n < N){
    
    float x_n = c_x[n];
    float r = c_r[n];
    for (int iter = 0; iter < Nit; iter++){
      // naive way is 3 flops, we can make this more efficient
      // per class instruction
      x_n = r * x_n * (1 - x_n);
    }

    c_x[n] = x_n;

  }

}

int  main(int argc, char **argv){

  cudaSetDevice(2);

  if(argc<3){
    printf("Execute: ./logisticMap N Nit\n");
    return -1;
  }

  // N and Nit are taken as command line arguments
  int N = atoi(argv[1]);
  int Nit = atoi(argv[2]);

  // ALLOCATE HOST ARRAYS
  float *h_x = (float*) calloc(N, sizeof(float));
  float *h_r = (float*) calloc(N, sizeof(float));

  // ALLOCATE DEVICE ARRAYS
  float *c_r;
  float *c_x;
  cudaMalloc(&c_r, N*sizeof(float));
  cudaMalloc(&c_x, N*sizeof(float));

  // set random seed
  srand48(time(NULL));

  // set h_x and h_r
  // h_x is random numbers between 0 and 1
  // h_r is evenly distributed from 0 to 4
  int n;
  float stepSize = 4.0 / (N - 1);
  for (n = 0; n < N; n++){
    h_x[n] = drand48();
    h_r[n] = n * stepSize;
  }

  // copy data from host to device
  cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_r, h_r, N*sizeof(float), cudaMemcpyHostToDevice);

  // specify launch configuration
  int B = 256;
  int G = (N + B - 1) / B;

  // do warm up, this will technically give us incorrect results
  // for x but we only really care about out throughput anyways
  int Nwarm = 100;
  for (n = 0; n < Nwarm; n++){
    logisticMap <<< G, B >>> (N, Nit, c_x, c_r);
  }

  // throughput measurement
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  
  cudaEventRecord(start);
  logisticMap <<< G, B >>> (N, Nit, c_x, c_r);
  cudaEventRecord(stop);

  cudaMemcpy(h_x, c_x, N*sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_r, c_r, N*sizeof(float), cudaMemcpyDeviceToHost);

  float elapsed;
  cudaEventElapsedTime(&elapsed, start, stop);
  elapsed /= 1000;

  unsigned long long int work = 0;
  int opsPerIteration = 3;
  
  // Each thread is doing opsPerIteration work Nit times
  work = opsPerIteration * N * Nit;

  double throughput = work / elapsed;
  throughput /= 1.e9; // GOP/s

  // print out results in csv format
  printf("r,x,iter\n");
  for (n = 0; n < N; n++){
    printf("%f,%f,%d\n", h_r[n], h_x[n], Nit);
  }

  //printf("%f\n", throughput); 

  cudaFree(c_r);
  cudaFree(c_x);
  free(h_r);
  free(h_x);
  
}


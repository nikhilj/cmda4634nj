/*

To compile:
gcc -O3  -o mandelbrot mandelbrot.c png_util.c -I. -lm -lpng

To run:
./mandelbrot

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
extern "C" 
{
#include "png_util.h"
}

__global__ void mandelbrot(const int Nx, 
		const int Ny, 
		const float minx,
		const float miny,
		const float hx, 
		const float hy,
		float * __restrict__ h_count){

  int tx = threadIdx.x; // x-coordinate in thread block
  int ty = threadIdx.y; // y-coordinate in thread block

  int bx = blockIdx.x;
  int by = blockIdx.y; // y-coordinate of block

  int dx = blockDim.x; // x-dimension of thread-block
  int dy = blockDim.y; // y-dimension of thread-block

  int nx = dx*bx + tx; // global x index of thread
  int ny = dy*by + ty; // global y index of thread


  int n = nx + ny*Nx;
  
  //
  if(nx<Nx && ny<Ny){    
      float cx = minx + nx*hx;
      float cy = miny + ny*hy;

      float zx = 0;
      float zy = 0;
      
      int Nt = 200;
      int t, cnt=0;
      for(t=0;t<Nt;++t){
	
	// z = z^2 + c
	//   = (zx + i*zy)*(zx + i*zy) + (cx + i*cy)
	//   = zx^2 - zy^2 + 2*i*zy*zx + cx + i*cy
	float zxTmp = 1/(zx*zx*zx*zx) - zy*zy + cx;
	zy = 2.f*zy*zx + cy;
	zx = zxTmp;

	cnt += (zx*zx+zy*zy<4.f);
      }

      h_count[n] = cnt;
    }
  
  

}


int main(int argc, char **argv){


  
  const int Nx = 4096;
  const int Ny = 4096;
  cudaSetDevice(2);  
  int D = 16;
  dim3 B(D, D);
  dim3 G( (Nx+D-1)/D, (Ny+D-1)/D);

  /* box containing sample points */
  const float centx = -.759856, centy= .125547;
  const float diam  = 0.151579;
  const float minx = centx-0.5*diam;
  const float remax = centx+0.5*diam;
  const float miny = centy-0.5*diam;
  const float immax = centy+0.5*diam;

  const float dx = (remax-minx)/(Nx-1.f);
  const float dy = (immax-miny)/(Ny-1.f);

  float *h_count = (float*) calloc(Nx*Ny, sizeof(float));
  float *c_count;
  cudaMalloc(&c_count, Nx*Ny*sizeof(float));
  
  cudaEvent_t start, stop;

  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  
  cudaEventRecord(start);

  // call mandelbrot from here
  mandelbrot <<< G, B >>> (Nx, Ny, minx, miny, dx, dy, c_count);
  
  cudaEventRecord(stop);
  
  cudaMemcpy(h_count, c_count, Nx*Ny*sizeof(float), cudaMemcpyDeviceToHost);
    
  float elapsedTime;
  cudaEventElapsedTime(&elapsedTime, start, stop);
  elapsedTime /= 1000;
  printf("Elapsed time: %f\n", elapsedTime);

  FILE *png = fopen("cudaMandelbrot.png", "w");
  write_hot_png(png, Nx, Ny, h_count, 0, 80);
  fclose(png);
  
  free(h_count);
  cudaFree(c_count);
}

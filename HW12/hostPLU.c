// original LU code:
// https://en.wikipedia.org/wiki/LU_decomposition

// pivoted version of hostPLU
void hostPLU(int N, int M, const dfloat *A, dfloat *LU, int *P, dfloat tol){

  // loop over matrices
  for(int n=0;n<N;++n){

    int i, j, k;

    // pointer to nth A and LU matrix storage
    const dfloat *An  = A +n*M*M;
    dfloat *LUn = LU+n*M*M;
    int    *Pn  = P +n*M; 

    // initialize pivot 
    for(int j=0;j<M;++j){
      Pn[j] = j;
    }    
    
    // i will be column
    // j will be row
    
    // note we assume column major storage
    for (j = 0; j < M; ++j) {
      for (i = 0; i < M; ++i) {
	LUn[j + M*i] = An[j + M*i];
      }
    }

    // loop over columns
    for (i = 0; i < M; ++i) {
      
      // find pivot row
      dfloat maxLU = 0.0, absLUji = 0.0;
      int imax = i;
      
      for (j = i; j < M; j++){
	// find entry in column i with largest abs value
	absLUji = fabs(LUn[j+i*M]);
	if (absLUji > maxLU) { 
	  maxLU = absLUji;
	  imax = j;
	}
      }
      
      if (maxLU < tol){ printf("hostPivotedLU broke down\n"); exit(-1);} //failure, matrix is degenerate
      
      if (imax != i) {
	//pivoting P
	j = Pn[i];
	Pn[i] = Pn[imax];
	Pn[imax] = j;

	//pivoting rows of A
	for(j=0;j<M;++j){
	  // swap rows i and imax
	  dfloat tmp = LUn[i+M*j];
	  LUn[i + M*j] = LUn[imax+M*j];
	  LUn[imax+M*j] = tmp;
	}
      }

      // now do LU decomposition on row permuted matrix
      // loop over rows starting from diagonal
      for (j = i + 1; j < M; ++j) {

	if(fabs(LUn[i*M+i])<tol){
	  printf("hostLU: matrix diagonal %g indicates LU breakdown, exiting\n",
		 LUn[i*M+i]);
	}
	
	LUn[j+M*i] /= LUn[i*M+i];
	
	for (k = i + 1; k < M; k++)
	  LUn[j+M*k] -= LUn[j+M*i] * LUn[i+M*k];
      }
    }
  }
}

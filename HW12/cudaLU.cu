#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include <time.h>

// N matrices to factorize
// matrices are MxM

#define dfloat float
#define p_M 16

void hostLU(int N, int M, dfloat *A, dfloat *LU){

  int i, j, k;

  for(int b=0;b<N;++b){
    dfloat Ab[p_M][p_M];

    for (i = 0; i < M; i++) {
      for(j=0;j<M;++j){
	int id = M*M*b + i*M + j;
	Ab[j][i] = A[id];
      }
    }
    
    for (i = 0; i < M; i++) {
      
      for (j = i + 1; j < M; j++) {
	Ab[j][i] /= Ab[i][i];
	
	for (k = i + 1; k < M; k++)
	  Ab[j][k] -= Ab[j][i] * Ab[i][k];
      }
    }
    
    for (i = 0; i < M; i++) {
      for(j=0;j<M;++j){
	int id = M*M*b + i*M + j;
	LU[id] = Ab[j][i];
      }
    }
  }
}

__global__ void __launch_bounds__(1) cudaLUv0(int N, int M, dfloat *A, dfloat *LU){

  int i, j, k;

  //  for(int b=0;b<N;++b)

  {
    int b = blockIdx.x;
    dfloat Ab[p_M][p_M];

    for (i = 0; i < M; i++) {
      for(j=0;j<M;++j){
	int id = M*M*b + i*M + j;
	Ab[j][i] = A[id];
      }
    }
    
    for (i = 0; i < M; i++) {
      
      for (j = i + 1; j < M; j++) {
	Ab[j][i] /= Ab[i][i];
	
	for (k = i + 1; k < M; k++)
	  Ab[j][k] -= Ab[j][i] * Ab[i][k];
      }
    }
    
    for (i = 0; i < M; i++) {
      for(j=0;j<M;++j){
	int id = M*M*b + i*M + j;
	LU[id] = Ab[j][i];
      }
    }
  }
}

// class version 1
__global__ void __launch_bounds__(1) cudaLUv1(int N, int M, dfloat *A, dfloat *LU){

  int i, j, k;

  //  for(int b=0;b<N;++b)

  {
    int b = blockIdx.x;
    __shared__ dfloat s_Ab[p_M][p_M];

    for (i = 0; i < M; i++) {
      for(j=0;j<M;++j){
	int id = M*M*b + i*M + j;
	s_Ab[j][i] = A[id];
      }
    }
    
    for (i = 0; i < M; i++) {
      // loop over columns
      for (i = 0; i < M; ++i) {
      
        // find pivot row
        dfloat maxLU = 0.0, absLUji = 0.0;
        int imax = i;
      
        for (j = i; j < M; j++){
	  // find entry in column i with largest abs value
	  absLUji = fabs(LUn[j+i*M]);
	  if (absLUji > maxLU) { 
	    maxLU = absLUji;
	    imax = j;
	  }
        }    
      
        if (maxLU < tol){ printf("hostPivotedLU broke down\n"); exit(-1);} //failure, matrix is degenerate
      
        if (imax != i) {
	  //pivoting P
	  j = Pn[i];
	  Pn[i] = Pn[imax];
	  Pn[imax] = j;

	  //pivoting rows of A
	  for(j=0;j<M;++j){
	    // swap rows i and imax
	    dfloat tmp = LUn[i+M*j];
	    LUn[i + M*j] = LUn[imax+M*j];
	    LUn[imax+M*j] = tmp;
	  }
        }  

        for (j = i + 1; j < M; j++) {
	  s_Ab[j][i] /= s_Ab[i][i];
	
	  for (k = i + 1; k < M; k++)
	    s_Ab[j][k] -= s_Ab[j][i] * s_Ab[i][k];
        } 
      }
    
      for (i = 0; i < M; i++) {
        for(j=0;j<M;++j){
	  int id = M*M*b + i*M + j;
	  LU[id] = s_Ab[j][i];
        }
      }
    }
  }


// class version 2
__global__ void __launch_bounds__(p_M) cudaLUv2(int N, int M, dfloat *A, dfloat *LU){

  int i, k;

  int j = threadIdx.x;
  {
    int b = blockIdx.x;
    volatile __shared__ dfloat s_Ab[p_M][p_M+1];

    for (i = 0; i < M; i++) {
      int id = M*M*b + i*M + j;
      s_Ab[j][i] = A[id];
    }
    
    for (i = 0; i < M; i++) {
      {
	//	__syncthreads();
	if(j>=i+1){
	  s_Ab[j][i] /= s_Ab[i][i];
	  
	  for (k = i + 1; k < M; k++)
	    s_Ab[j][k] -= s_Ab[j][i] * s_Ab[i][k];
	}
      }
    }
    
    for (i = 0; i < M; i++) {
      int id = M*M*b + i*M + j;
      LU[id] = s_Ab[j][i];
    }
  }
}

// class version 3 - shfl
__global__ void __launch_bounds__(p_M) cudaLUv3(int N, int M, dfloat *A, dfloat *LU){

  int i, k;

  int j = threadIdx.x;
  {
    int b = blockIdx.x;
    //    volatile __shared__ dfloat s_Ab[p_M][p_M+1];
    dfloat r_Ab[p_M];

    for (i = 0; i < p_M; i++) {
      int id = p_M*p_M*b + i*p_M + j;
      //      s_Ab[j][i] = A[id];
      r_Ab[i] = A[id];
    }

#define mask 0xffffffff
#define width 32

#pragma unroll 
    for (i = 0; i < p_M; i++) {
      {
	dfloat Aii = __shfl_sync (mask, r_Ab[i], i, width);
	
	if(j>=i+1){
	  r_Ab[i] /= Aii;
	}
	for (k = i + 1; k < p_M; k++){
	  dfloat Aik = __shfl_sync (mask, r_Ab[i], k, width);
	  if(j>=i+1){
	    r_Ab[k] -= r_Ab[i] * Aik;
	  }
	}
      }
    }
    
    for (i = 0; i < p_M; i++) {
      int id = p_M*p_M*b + i*p_M + j;
      LU[id] = r_Ab[i];
    }
  }
}

__global__ void cudaCopy(int N, dfloat *A, dfloat *B){
  int n = threadIdx.x + blockIdx.x*blockDim.x;
  if(n<N){
    B[n] = A[n];
  }
}
	   

void launcher(int op, int N, int M, dfloat *c_A, dfloat *c_LU){

  cudaEvent_t tic, toc;
  cudaEventCreate(&tic);
  cudaEventCreate(&toc);


  cudaEventRecord(tic);
  switch(op){
  case 0:{
    cudaLUv0 <<< N, 1  >>> (N, M, c_A, c_LU);
    break;
  }
  case 1:{
    cudaLUv1 <<< N, 1  >>> (N, M, c_A, c_LU);
  }
  case 2:{
    cudaLUv2 <<< N, M  >>> (N, M, c_A, c_LU);
  }
  case 3:{
    cudaLUv3 <<< N, M  >>> (N, M, c_A, c_LU);
  }
  }

  cudaEventRecord(toc);
  cudaEventSynchronize(toc);

  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /=1000;

  printf("kernel %d took %g\n", op, elapsed);
}


int main(int argc, char **argv){

  int N = atoi(argv[1]);
  int M = p_M;

  size_t sz = N*M*M*sizeof(dfloat);
  dfloat *h_A = (dfloat*) malloc(sz);
  dfloat *h_LU = (dfloat*) malloc(sz);

  clock_t tic, toc;

  tic = clock();
  hostLU(N, M, h_A, h_LU);
  toc = clock();

  float elapsed = (toc-tic)/(double)CLOCKS_PER_SEC;
  printf("elapsed = %g\n", elapsed);

  dfloat *c_A, *c_LU;
  cudaMalloc(&c_A, sz);
  cudaMalloc(&c_LU, sz);
  cudaMemcpy(c_A, h_A, sz, cudaMemcpyHostToDevice);

  launcher(0, N, M, c_A, c_LU);
  launcher(1, N, M, c_A, c_LU);
  launcher(2, N, M, c_A, c_LU);
  launcher(3, N, M, c_A, c_LU);
  
  cudaMemcpy(h_LU, c_LU, sz, cudaMemcpyDeviceToHost);

  int G = (N*p_M*p_M+255)/256;
  int B = 256;
  cudaCopy <<< G, B >>> (N*p_M*p_M, c_A, c_LU);

  cudaDeviceSynchronize();
  
}

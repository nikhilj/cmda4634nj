
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <cuda.h>

#define p_B 64

// shared memory
__global__ void partialScanKernelV0(int N, float *x, float *scanx, float *endsx){

  __shared__ float s_x1[p_B];
  __shared__ float s_x2[p_B];

  int t = threadIdx.x;
  int b = blockIdx.x;

  int n = t + b*p_B;

  s_x1[t] = (n<N) ? x[n] : 0;
  
  __syncthreads();

  s_x2[t] = (t>0) ? s_x1[t] + s_x1[t-1] : s_x1[t];

  __syncthreads();

  s_x1[t] = (t>1) ? s_x2[t] + s_x2[t-2] : s_x2[t];

  __syncthreads();

  s_x2[t] = (t>3) ? s_x1[t] + s_x1[t-4] : s_x1[t];

  __syncthreads();

  s_x1[t] = (t>7) ? s_x2[t] + s_x2[t-8] : s_x2[t];

  __syncthreads();

  s_x2[t] = (t>15) ? s_x1[t] + s_x1[t-16] : s_x1[t];

  __syncthreads();

  float res = (t>31) ? s_x2[t] + s_x2[t-32] : s_x2[t];
  if(n<N){
    scanx[n] = res;
  }

  if(t==p_B-1){
    endsx[b] = res;
  }
}

__global__ void scan(int N, float *scanx, float* endsx){
	int t = threadIdx.x;
	int b = blockIdx.x;
	int n = t + b*p_B;

	if (b>0)
		scanx[n] += endsx[b-1];

}

int main(int argc, char** argv){
	int N = atoi(argv[1]);
	int G = (N+p_B-1)/p_B;
	int n;
	float *h_x = calloc(N, sizeof(float));
	float *h_scanx = calloc(N, sizeof(float));
	float *h_endx = calloc(N, sizeof(float));
	float *c_x, *c_scanx, *c_endsx;
	cudaMalloc(&c_x, N*sizeof(float));
	cudaMalloc(&c_scanx, N*sizeof(float));
	cudaMalloc(&c_endsx, N*sizeof(float));
	for (n=0;n<N;++n)
		h_x[n] = 1;
	cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);

	partialScanKernelV0 <<< G, p_B >>> (N, c_x, c_scanx, c_endsx);

	cudaMemcpy(h_endsx, c_endsx, N*sizeof(float), cudaMemcpyHostToDevice);
	for (i=1;i<G;++i)
		h_endsx[i] += h_endsx[i-1];
	scan <<< G, p_B >>> (N, c_scanx, c_endsx);
	cudaMemcpy(h_scanx, c_scanx, N*sizeof(float), cudaMemcpyDeviceToHost);

}

